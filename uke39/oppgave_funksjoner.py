# Del 1:
# Lag en funksjon differanse(tall1, tall2)
# Den skal _returnere_ differansen mellom tallene,
# absoluttverdien
# Inntil videre kaller vi bare denne ved at du skriver ut
# resultatet av et kall til differanse() med to valgfrie tall

# Del 2:
# Lag en funksjon tilfeldig_tall som spør brukeren om
# maksgrensen. Funksjonen skal returnere et tilfeldig tall
# mellom 0 og dette tallet.

# Del 3
# Lag funksjonen main()
# Den skal hente inn to tilfeldige tall med
# maksgrense gitt over. Så skal den skrive
# ut tallene og absoluttverdien av differansen.
# kjør så main()