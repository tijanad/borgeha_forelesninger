
    # Denne under tar du hjemme, den tar for mye tid nå.
    # spør hvor stort hopp du skal gjøre mellom hvert tegn
    # for hvert hopp gjennom strengen skriver du ut om bokstaven er en vokal:
    # bokstav in 'aeiouyæøå'
# Det siste: break og continue
# break kan presse seg ut av en løkke, HELT ut
"""
nok = 0
while True:
    break
    if nok > 1000:
        print('nå er det nok!')
        break
    nok += 1
# etter dette    
print(nok)
"""
"""
# Oppgave: (Neppe på forelesning, men dere kan gjøre den selv)
# Kan dere lage en evig løkke, som spør om navn,
# og det gjør den helt til navnet gitt har to a'er?

# continue presser seg ut av denne 'iterasjonen' gjennom løkka, og starter øverst igjen
"""
# Skriv ut tall delelige på alle av 3, 7 og 13:
# Hvis det ikke er delelig på 13, da kan vi hoppe rett ut med en gang
for i in range(1,1000):
    if i % 13 == 0:
        print(f"{i} er delelig på 13")
        continue
    if i % 7 == 0:
        print(f"{i} er delelig på 7")
        continue
    if i % 3 == 0:
        print(f"{i} er delelig på 3")
        continue
    print(i)
