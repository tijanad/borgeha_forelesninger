# Gå til bb, gamle eksamener, fyr opp kont2018
# Den med passord og fotballskår.

notebook = {}

def addSite(notebook):
    site = input('Site: ')
    if site in notebook:
        print(f'{site} er allerede lagret.')
        return notebook
    username = input("brukernavn: ")
    password = input("passord: ")
    notebook[site] = [username,[password]]
    return notebook

notebook = {'facebook': ['urgle', ['pwd','newpass']], \
            'vgeretfrykteligkortnavn': ['user', ['pwd2']], \
            'db': ['nah', ['pwd222']]}

def showSites(notebook):
    print(f"{'Nettsted:':17}{'Brukernavn:':15}{'Passord:'}")
    for site, values in notebook.items():
        print(f"{site[:15]:17}{values[0][:15]:15}{values[1][-1]}")
    
showSites(notebook)

def formatList(liste):
    return ", ".join(liste)

print(formatList([str(i) for i in range(10)]))

def editSite(notebook, site):
    while True:
        password = input(f'Add new password for {site}: ')
        if password in notebook[site][1]:
            print(f"'{password}' has been used for {site} already.")
            print(f'The following passwords have been used: {formatList(notebook[site][1])}.')
        else:
            notebook[site][1].append(password)
            return notebook
            
#editSite(notebook, 'facebook')
        
def secureSites(notebook):
    passFrequency = {}
    for site, val in notebook.items():
        lastpass = val[-1][-1]
        usedinsites = passFrequency.get(lastpass, [])
        usedinsites.append(site)
        passFrequency[lastpass] = usedinsites
    
    allUnique = True        

    for password, sites in passFrequency.items():

        if len(sites) > 1:
            print(f'You have used {password} at these sites: {formatList(sites)}.')
            allUnique = False
        
    if allUnique:        
        print("Du er så flink atter!")

secureSites(notebook)
    